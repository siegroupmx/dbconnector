<?php
class DBConnector {
	const MYSQL_PORT= 3306; # default MySQL or MySQLi port
	const MSSQL_PORT= 1433; # default Microsoft SQL Server port
	const POSTGRESQL_PORT= 5432; # default PostgreSQL port

	private $dbLink=array();
	private $error=NULL;
	private $error_code=NULL;
	private $sucess=NULL;
	private $sucess_code=NULL;
	private $dbList= array( "mysql", "mysqli", "mssql", "psql" );
	private $dbEngine=NULL;
	private $dbUser=NULL;
	private $dbPass=NULL;
	private $dbName=NULL;
	private $dbHost=NULL;
	private $dbPort=NULL;
	private $dbResource=NULL;
	private $query=NULL;
	private $qResult=NULL;
	private $lowerCaseTableNames=false;
	private $indexName=NULL;

	/**
	* valida existencia del nombre del indice
	*
	* @return boolean falso verdadero
	*/
	public function existIndexName() {
		return ($this->indexName ? true:false);
	}

	/**
	* establece el nombre del indice
	*
	* @param string $a el nombre del indice
	*/
	public function setIndexName($a=NULL) {
		$this->indexName=$a;
	}

	/**
	* integra el uso del indice
	*
	* @return string la query del indice
	*/
	public function useIndexName() {
		$r="";
		if( $this->indexName ) {
			$r= " USE INDEX(".$this->indexName. ")";
		}
		return $r;
	}

	/**
	* elimina valores de cache
	*
	* @param string nombre de la conexion
	*/
	public function cleanDbCache($tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);

		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else { # nueva conexion
			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				mysql_free_result($this->query);
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				mysqli_free_result($this->query);
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				$this->query=NULL;
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				$this->query=NULL;
			}
			return false;
		}
	}

	/**
	* Habilita que los nombres de las tablas sean convertidos a minuscula, por defceto falso
	*
	* @param boolean $a verdadero o falso
	*/
	public function setLowercaseTableNames($a=false) {
		$this->lowerCaseTableNames= $a;
	}

	/**
	* Devuelve la configuracion para los nombres de las tablas
	*
	* @return boolean verdadero o falso
	*/
	public function isLowercaseTableNamesEnable() {
		return $this->lowerCaseTableNames;
	}

	/**
	* Devuelve el nombre de a tabla regulado a la configuracion de setLowercaseTableNames()
	*
	* @return string nombre dela tabla satizidado
	*/
	public function sanitizeTableName($a=NULL) {
		if( $this->isLowercaseTableNamesEnable() ) { // habilitado minusculas forzoso
			return strtolower($a);
		}
		else { // no habilitado, retorna normal
			return $a;
		}
	}

	/**
	* cierra la conexion a la base de datos
	*
	* @param string nombre de la conexion
	*/
	public function closeDb($tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);

		if( !$this->isDataNeededToConnect() )	return false;
		else { # nueva conexion
			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				mysql_close($this->dbLink[$tag]);
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				mysqli_close($this->dbLink[$tag]);
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				sqlsrv_close($this->dbLink[$tag]);
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				$this->dbLink[$tag]=NULL;
			}
			return false;
		}
	}

	/**
	* verifica si los datos necesarios para conectar a la base de datos existen
	*
	* @return boolean falso o verdadeor de la validacion
	*/
	public function isDataNeededToConnect() {
		if( !$this->getUser() ) {
			$this->setError("no indico el nombre de usuario", 5);
			return false;
		}
		else if( !$this->getPass() ) {
			$this->setError("no indico el password", 6);
			return false;
		}
		else if( !$this->getHost() ) {
			$this->setError("no indico el nombre del host o IP", 4);
			return false;
		}
		else if( !$this->getPort() ) {
			$this->setError("no indico el puerto para la conexion", 11);
			return false;
		}
		else if( !$this->getDbName() ) {
			$this->setError("no indico el nombre de la base de datos", 7);
			return false;
		}
		else {
			$this->setError(NULL, NULL);
			$this->setSucess("conexion a la base realizada con exito", 1);
			return true;
		}
	}

	/**
	* conectar al motor de base de datos
	*
	* @param string nombre de la conexion
	*/
	public function conectar($tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);

		if( !$this->isDataNeededToConnect() )	return false;
		else if( isset($this->dbLink[$tag]) ) 	return true; # ya existe esa conexion
		else { # nueva conexion
			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($link= mysql_connect($this->getHost().($this->getPort() ? ':'.$this->getPort():''), $this->getUser(), $this->getPass())) )
					$this->setError("problema para conectar a la base, razon: ".mysql_error(), 8);
				else if( !mysql_select_db($this->getDbName(), $link) )
					$this->setError("problema para seleccionar la base de datos, razon: ".mysql_error(), 9);
				else {
					$this->dbLink[$tag]= $link;
					return true;
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($link=mysqli_connect($this->getHost().($this->getPort() ? ':'.$this->getPort():''), $this->getUser(), $this->getPass(), $this->getDbName())) )
					$this->setError("problema para conectar a la base, razon: ".mysqli_connect_errno(), 8);
				else {
					$this->dbLink[$tag]= $link;
					return true;
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				$servername= $this->getHost().$this->getDbResource().($this->getPort() ? ', '.$this->getPort():'');
				$database= array("Database"=>$this->getDbName(), "UID"=>$this->getUser(), "PWD"=>$this->getPass());
				if( !($link=sqlsrv_connect($servername, $database)) )
					$this->setError("problema para conectar a la base, razon: ".$this->getMSSqlError(sqlsrv_errors()), 8);
				else {
					$this->dbLink[$tag]= $link;
					return true;
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				$link= new PDO('pgsql:dbname='. $this->getDbName(). ';host='. $this->getHost().($this->getPort() ? ';port='.$this->getPort():''), $this->getUser(), $this->getPass());
				#if( PDOException $e )
				#	$this->setError("problema para conectar a la base, razon: ".$e->getMessage(), 8);
				#else {
					$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$this->dbLink[$tag]= $link;
					return true;
				#}
			}
			return false;
		}
	}

	/**
	* onefloor - consultar
	*/
	public function consultar($tabla=NULL, $a=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$a ) {
			return false;
		}
		else {
			$this->query(false, $this->sanitizeTableName($tabla), $a);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar con
	*/
	public function consultar_con($tabla=NULL, $query=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$query ) {
			return false;
		}
		else {
			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, $query);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar enorden
	*/
	public function consultar_enorden($tabla=NULL, $orden=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$orden ) {
			return false;
		}
		else {
			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, NULL, $orden);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar enorden con
	*/
	public function consultar_enorden_con($tabla=NULL, $cond=NULL, $orden=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$orden || !$cond ) {
			return false;
		}
		else {
			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, $cond, $orden);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar rando enorden
	*/
	public function consultar_rango_enorden( $tabla=NULL, $valor=NULL, $fechainicio=NULL, $fechafin=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || $valor || !$fechainicio || !$fechafin ) {
			return false;
		}
		else {
			$between= array(
				"target"=>$valor, 
				"from"=>$fechainicio, 
				"to"=>$fechafin
			);
			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, NULL, NULL, $between);
			unset($between);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar rando enorden con
	*/
	public function consultar_rango_enorden_con( $tabla=NULL, $cond=NULL, $valor=NULL, $fechainicio=NULL, $fechafin=NULL, $orden=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$valor || !$cond || !$fechainicio || !$fechafin || !$orden ) {
			return false;
		}
		else {
			$between= array(
				"target"=>$valor, 
				"from"=>$fechainicio, 
				"to"=>$fechafin
			);
			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, $cond, $orden, $between);
			unset($between);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar rando limite en orden con
	*/
	public function consultar_rango_limite_enorden_con( $tabla=NULL, $valor=NULL, $limite=NULL, $fechainicio=NULL, $fechafin=NULL, $orden=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$valor || !$fechainicio || !$fechafin || !$orden ) {
			return false;
		}
		else {
			$between= array(
				"target"=>$valor, 
				"from"=>$fechainicio, 
				"to"=>$fechafin
			);

			if( is_array($limite) ) { # es arreglo
				$limit= array(
					"from"=>$limite[0], # tomamos nodo
					"to"=>$limite[1] # tomamos nodo
				);
			}
			else { # no es arreglo, viene delimitado
				$x= explode( ",", $limite); # partimos
				$limit= array(
					"from"=>$x[0], # tomamos nodo
					"to"=>$x[1] # tomamos nodo
				);
			}

			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, NULL, $orden, $between, NULL, $limit);
			unset($between, $limit);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar indexados
	*/
	public function consultar_indexados($tabla=NULL, $target=NULL, $muestra=NULL, $thisSel="*", $indexName=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$target || !$muestra ) {
			return false;
		}
		else {
			$index=array(
				"target"=>$target, 
				"data"=>$muestra
			);
			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, NULL, NULL, NULL, $index);
			unset($index);
			return $this->query;
		}
	}

	/**
	* onefloor - contar indexados y obtener TOTAL
	*/
	public function contar_indexados($tabla=NULL, $target=NULL, $muestra=NULL, $thisSel="*", $indexName=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() )	return false;
		else if( !$tabla || !$target || !$muestra ) 	return false;
		else {
			$index=array(
				"target"=>$target, 
				"data"=>$muestra
			);
			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), "COUNT(".$thisSel.") as TOTAL", NULL, NULL, NULL, $index);
			unset($index);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar indexados en orden
	*/
	public function consultar_indexados_enorden($tabla=NULL, $target=NULL, $muestra=NULL, $orden=NULL, $thisSel="*", $indexName=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$target || !$muestra || !$orden ) {
			return false;
		}
		else {
			$index=array(
				"target"=>$target, 
				"data"=>$muestra
			);
			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, NULL, $orden, NULL, $index);
			unset($index);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar limite
	*/
	public function consultar_limite($tabla=NULL, $limite=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$limite ) {
			return false;
		}
		else {
			if( is_array($limite) ) { # es arreglo
				$limit= array(
					"from"=>$limite[0], # tomamos nodo
					"to"=>$limite[1] # tomamos nodo
				);
			}
			else { # no es arreglo, viene delimitado
				$x= explode( ",", $limite); # partimos
				$limit= array(
					"from"=>$x[0], # tomamos nodo
					"to"=>$x[1] # tomamos nodo
				);
			}

			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, NULL, NULL, NULL, NULL, $limit);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar limite con
	*/
	public function consultar_limite_con($tabla=NULL, $cond=NULL, $limite=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$cond || !$limite ) {
			return false;
		}
		else {
			if( is_array($limite) ) { # es arreglo
				$limit= array(
					"from"=>$limite[0], # tomamos nodo
					"to"=>$limite[1] # tomamos nodo
				);
			}
			else { # no es arreglo, viene delimitado
				$x= explode( ",", $limite); # partimos
				$limit= array(
					"from"=>$x[0], # tomamos nodo
					"to"=>$x[1] # tomamos nodo
				);
			}

			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, $cond, NULL, NULL, NULL, $limit);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar limite enorden
	*/
	public function consultar_limite_enorden($tabla=NULL, $limite=NULL, $orden=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$orden || !$limite ) {
			return false;
		}
		else {
			if( is_array($limite) ) { # es arreglo
				$limit= array(
					"from"=>$limite[0], # tomamos nodo
					"to"=>$limite[1] # tomamos nodo
				);
			}
			else { # no es arreglo, viene delimitado
				$x= explode( ",", $limite); # partimos
				$limit= array(
					"from"=>$x[0], # tomamos nodo
					"to"=>$x[1] # tomamos nodo
				);
			}

			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, NULL, $orden, NULL, NULL, $limit);
			return $this->query;
		}
	}

	/**
	* onefloor - consultar limite enorden con
	*/
	public function consultar_limite_enorden_con($tabla=NULL, $cond=NULL, $limite=NULL, $orden=NULL, $thisSel="*", $indexName=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$orden || !$cond || !$limite ) {
			return false;
		}
		else {
			if( is_array($limite) ) { # es arreglo
				$limit= array(
					"from"=>$limite[0], # tomamos nodo
					"to"=>$limite[1] # tomamos nodo
				);
			}
			else { # no es arreglo, viene delimitado
				$x= explode( ",", $limite); # partimos
				$limit= array(
					"from"=>$x[0], # tomamos nodo
					"to"=>$x[1] # tomamos nodo
				);
			}

			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $thisSel, $cond, $orden, NULL, NULL, $limit);
			return $this->query;
		}
	}

	/**
	* inserta en la base de datos
	*
	* @param string nombre de la tabla
	* @param array/string elementos a utilizar en el insert
	* @param boolen element para determinar el uso del autoconvertidor
	* @param string nombre de la conexion
	*/
	public function insertar_bdd($tabla=NULL, $args=NULL, $autoconvert=true, $tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else {
			$in= ($autoconvert ? $this->arr2data($args, "insert"):$args); # convertir a insertables
			$q= 'insert into '. $this->sanitizeTableName($tabla). ' ('. $in["data"]. ') values('. $in["values"]. ')';

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($this->query= mysql_query($q, $this->dbLink[$tag])) ) {
					return 0;
				}
				else { # exito de consultar
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($this->query=mysqli_query($this->dbLink[$tag], $q)) ) {
					return 0;
				}
				else { # exito de consulta
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				if( !($this->query=sqlsrv_query($this->dbLink[$tag], $q)) )
					return 0;
				else { # exito de consulta
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				if( !($this->query= $this->dbLink[$tag]->query($q)) ) {
					return 0;
				}
				else { # exito de consulta
					return 1;
				}
			}
			else {
				return 0;
			}
		}
	}

	/**
	* actualiza en la base de datos
	*
	* @param string nombre de la tabla
	* @param array/string elementos a utilizar en el insert
	* @param boolen element para determinar el uso del autoconvertidor
	* @param string nombre de la conexion
	*/
	public function actualizar_bdd($tabla=NULL, $args=NULL, $autoconvert=true, $tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else {
			$in= ($autoconvert ? $this->arr2data($args, "update"):$args); # convertir a actualizable
			$q= 'update '. $this->sanitizeTableName($tabla). ' set '. $in["values"]. ' where '. $in["condition"];

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($this->query= mysql_query($q, $this->dbLink[$tag])) ) {
					return 0;
				}
				else { # exito de consultar
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($this->query=mysqli_query($this->dbLink[$tag], $q)) ) {
					$this->setError("problemas para realizar consulta, razon: ". mysqli_error($this->dbLink[$tag]), 10);
					return 0;
				}
				else { # exito de consulta
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				if( !($this->query=sqlsrv_query($this->dbLink[$tag], $q)) ) {
					return 0;
				}
				else { # exito de consulta
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				if( !($this->query= $this->dbLink[$tag]->query($q)) ) {
					return 0;
				}
				else { # exito de consulta
					return 1;
				}
			}
			else {
				return 0;
			}
		}
	}

	/**
	* elimina en la base de datos
	*
	* @param string nombre de la tabla
	* @param array/string elementos a utilizar en el insert
	* @param boolen element para determinar el uso del autoconvertidor
	* @param string nombre de la conexion
	*/
	public function eliminar_bdd($tabla=NULL, $args=NULL, $autoconvert=true, $tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else {
			$in= ($autoconvert ? $this->arr2data($args, "delete"):$args); # convertir a eliminable
			$q= 'delete from '. $this->sanitizeTableName($tabla). ' where '. $in["values"];

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($this->query= mysql_query($q, $this->dbLink[$tag])) ) {
					return 0;
				}
				else { # exito de consultar
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($this->query=mysqli_query($this->dbLink[$tag], $q)) ) {
					$this->setError("problemas para realizar consulta, razon: ". mysqli_error($this->dbLink[$tag]), 10);
					return 0;
				}
				else { # exito de consulta
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				if( !($this->query=sqlsrv_query($this->dbLink[$tag], $q)) ) {
					return 0;
				}
				else { # exito de consulta
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				if( !($this->query= $this->dbLink[$tag]->query($q)) ) {
					return 0;
				}
				else { # exito de consulta
					return 1;
				}
			}
			else {
				return 0;
			}
		}
	}

	/**
	* onefloor - consultar datos general
	*/
	public function consultar_datos_general($tabla=NULL, $cond=NUL, $target=NULL, $indexName=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$query ) {
			return false;
		}
		else {
			$this->setIndexName($indexName);
			$this->query(false, $this->sanitizeTableName($tabla), $target, $cond);
			$a= $this->getResult(); # regresamos respuesta
			return $a[$target];
		}
	}

	/**
	* onefloor - consultar con
	*/
	public function contador_celdas($tabla=NULL, $query=NULL ) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else if( !$tabla || !$query ) {
			return false;
		}
		else {
			$this->query(false, $this->sanitizeTableName($tabla), "*", $query);

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				return mysql_num_rows($this->query);
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				return mysqli_num_rows($this->query);
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				return sqlsrv_num_rows($this->query);
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				$this->rawQuery("select row_number() from ". $this->sanitizeTableName($tabla));
				return $this->getResult();
			}
		}
	}

	/**
	* ejecuta una consulta y guarda resultado en $qResult
	*
	* @param string nombre de la conexion
	* @param string nombre de la tabla
	* @param string elementos a seleccionar
	* @param array arreglo de condiciones
	* @param string ordenamiento de resultados
	* @param array arreglo de elementos entre valores
	* @param array elementos que asocien al requerimiento
	* @param array arreglo elementos del limite a consultar
	*/
	public function query($tagname=NULL, $tabla=NULL, $args=NULL, $cond=NULL, $order=NULL, $between=array(), $index=array(), $limit=array()) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else {
			$q= 'select '. ($args ? $args:'*'). # elementos a seleccionar
				' from '. $this->sanitizeTableName($tabla). # tabla
				($this->existIndexName() ? $this->useIndexName():"").
				((is_array($cond) && count($cond)) ? ' where '.$this->arr2data($cond):($cond ? ' where '. $cond:'')). # condicionales
				((is_array($between) && count($between)) ? (((is_array($cond) && count($cond)) ? ' AND ':' where '). $between["target"]. ' BETWEEN '. $between["from"]. ' AND '. $between["to"]):''). # entre elementos
				((is_array($index) && count($index)) ? ((is_array($cond) && count($cond)) ? ' AND ':''). $index["target"]. ' LIKE \'%'. $index["data"]. '%\'':''). # entre elementos
				($order ? ' ORDER BY '. $order:''). # orden
				((is_array($limit) && count($limit)) ? ' LIMIT '. $limit["from"]. ','. $limit["to"]:'') # limite
				;

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($this->query= mysql_query($q, $this->dbLink[$tag])) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ". mysql_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consultar
					if( !mysql_num_rows($this->query) ) {
						$this->setError("la busqueda resulto vacia o sin exito", 12);
						$this->setResult(NULL, NULL);
					}
					else {
						$this->setSucess("consultar realizada con exito", 2);
						$this->setResult($this->query);
					}
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($this->query=mysqli_query($this->dbLink[$tag], $q)) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ". mysqli_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consulta
					if( !mysqli_num_rows($this->query) ) {
						$this->setError("la busqueda resulto vacia o sin exito", 12);
						$this->setResult(NULL, NULL);
					}
					else {
						$this->setSucess("consultar realizada con exito", 2);
						$this->setResult($this->query);
					}
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				if( !($this->query=sqlsrv_query($this->dbLink[$tag], $q)) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ". $this->getMSSqlError(sqlsrv_errors()), 10);
				}
				else { # exito de consulta
					if( !sqlsrv_num_rows($this->query) ) {
						$this->setError("la busqueda resulto vacia o sin exito", 12);
						$this->setResult(NULL, NULL);
					}
					else {
						$this->setSucess("consultar realizada con exito", 2);
						$this->setResult($this->query);
					}
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				if( !($this->query= $this->dbLink[$tag]->query($q)) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ", 10);
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
					$this->setResult($this->query);
				}
			}
			else {
				$this->setError("el motor de base de datos indicado, no es soportado", 2);
			}
		}
	}

	/**
	* genera una consulta completamente manual, lo que el usuario indica es lo que se envia
	*
	* @param string la query directa
	* @param string nombre de la conexion
	*/
	public function rawQuery($a=NULL, $tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else {
			$q= $a;

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($this->query= mysql_query($q, $this->dbLink[$tag])) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ". mysql_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consultar
					if( !mysql_num_rows($this->query) ) {
						$this->setError("la busqueda resulto vacia o sin exito", 12);
						$this->setResult(NULL, NULL);
					}
					else {
						$this->setSucess("consultar realizada con exito", 2);
						$this->setResult($this->query);
					}
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($this->query=mysqli_query($this->dbLink[$tag], $q)) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ". mysqli_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consulta
					if( !mysqli_num_rows($this->query) ) {
						$this->setError("la busqueda resulto vacia o sin exito", 12);
						$this->setResult(NULL, NULL);
					}
					else {
						$this->setSucess("consultar realizada con exito", 2);
						$this->setResult($this->query);
					}
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				if( !($this->query=sqlsrv_query($this->dbLink[$tag], $q)) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ". $this->getMSSqlError(sqlsrv_errors()), 10);
				}
				else { # exito de consulta
					if( !sqlsrv_num_rows($this->query) ) {
						$this->setError("la busqueda resulto vacia o sin exito", 12);
						$this->setResult(NULL, NULL);
					}
					else {
						$this->setSucess("consultar realizada con exito", 2);
						$this->setResult($this->query);
					}
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				if( !($this->query= $this->dbLink[$tag]->query($q)) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ", 10);
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
					$this->setResult($this->query);
				}
			}
			else {
				$this->setError("el motor de base de datos indicado, no es soportado", 2);
			}
		}
	}

	/**
	* busca que el elemento dentro de una tabla
	*
	* @param string nombre de la tabla
	* @param array arreglo de condiciones
	* @param string variable o celda que necesitamos
	* @param string nombre de la conexion
	*/
	public function search($tabla=NULL, $cond=NULL, $need=NULL, $order=NULL, $between=NULL, $tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else {
			$q= 'select '. $need. ' from '. $this->sanitizeTableName($tabla). # tabla
				($this->existIndexName() ? $this->useIndexName():"").
				((is_array($cond) && count($cond)) ? ' where '. $this->arr2data($cond):''). # condicionales
				((is_array($between) && count($between)) ? ((is_array($cond) && count($cond)) ? ' AND ':' where '). $between["target"]. ' BETWEEN '. $between["from"]. ' AND '. $between["to"]:''). # entre elementos
				($order ? ' ORDER BY '. $order:'') # orden
				;

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($this->query= mysql_query($q, $this->dbLink[$tag])) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ". mysql_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consultar
					if( !mysql_num_rows($this->query) ) {
						$this->setError("la busqueda resulto vacia o sin exito", 12);
						$this->setResult(NULL, NULL);
					}
					else {
						$this->setSucess("busqueda de consultar realizada con exito", 2);
						$this->setResult($this->query, $need);
					}
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($this->query=mysqli_query($this->dbLink[$tag], $q)) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ". mysqli_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consulta
					if( !mysqli_num_rows($this->query) ) {
						$this->setError("la busqueda resulto vacia o sin exito", 12);
						$this->setResult(NULL, NULL);
					}
					else {
						$this->setSucess("busqueda de consultar realizada con exito", 2);
						$this->setResult($this->query, $need);
					}
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				if( !($this->query=sqlsrv_query($this->dbLink[$tag], $q)) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ". $this->getMSSqlError(sqlsrv_errors()), 10);
				}
				else { # exito de consulta
					if( !sqlsrv_num_rows($this->query) ) {
						$this->setError("la busqueda resulto vacia o sin exito", 12);
						$this->setResult(NULL, NULL);
					}
					else {
						$this->setSucess("busqueda de consultar realizada con exito", 2);
						$this->setResult($this->query, $need);
					}
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				if( !($this->query= $this->dbLink[$tag]->query($q)) ) {
					$this->setResult(NULL, true);
					$this->setError("problemas para realizar consulta, razon: ", 10);
				}
				else { # exito de consulta
					$this->setSucess("busqueda de consultar realizada con exito", 2);
					$this->setResult($this->query, $need);
				}
			}
			else {
				$this->setError("el motor de base de datos indicado, no es soportado", 2);
			}
		}
	}

	/**
	* establece resultado de una consulta
	*
	* @param array arreglo de datos a guardar
	* @param boolean indicador que solo guardaremos el primer elementos
	*/
	public function setResult($a=array(), $firtElement=false) {
		if( $a==NULL ) {
			$this->qResult= NULL;
			$this->setError("la busqueda resulto vacia o sin exito", 12);
		}
		else {
			if( !strcmp($this->getDbEngine(), "mysql") && mysql_num_rows($a) ) { # MySQL
				$aux=array();
				if( !$firtElement ) {
					while($b=mysql_fetch_array($a)) {
						$tmp=array(); # inicializamos
						foreach( $b as $key=>$val ) {
							if( !is_numeric($key) ) 	$tmp[$key]= $val; # obtenemos
						}
						$aux[]= $tmp; # anidamos
					}
				}
				else { # solo el primer elemento
					$b= mysql_fetch_array($a);
					$tmp=array();
					foreach( $b as $key=>$val ) {
						if( !is_numeric($key) ) 	$tmp[$key]= $val; # obtenemos
					}
					$aux[]= $tmp; # anidamos
				}
				$this->qResult= ((is_array($aux) && count($aux)) ? ($firtElement ? $aux[0][$firtElement]:$aux):NULL);
				unset($aux);
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") && mysqli_num_rows($a) ) { # MySQLi
				$aux=array();
				if( !$firtElement ) {
					while($b=mysqli_fetch_array($a)) {
						$tmp=array(); # inicializamos
						foreach( $b as $key=>$val ) {
							if( !is_numeric($key) ) 	$tmp[$key]= $val; # obtenemos
						}
						$aux[]= $tmp; # anidamos
					}
				}
				else { # solo el primer elemento
					$b= mysqli_fetch_array($a);
					$tmp=array();
					foreach( $b as $key=>$val ) {
						if( !is_numeric($key) ) 	$tmp[$key]= $val; # obtenemos
					}
					$aux[]= $tmp; # anidamos
				}
				$this->qResult= ((is_array($aux) && count($aux)) ? ($firtElement ? $aux[0][$firtElement]:$aux):NULL);
				unset($aux);
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				$aux=array();
				if( !$firtElement ) {
					while($b=sqlsrv_fetch_array($this->query, SQLSRV_FETCH_ASSOC)) {
						$tmp=array(); # inicializamos
						foreach( $b as $key=>$val ) {
							if( !is_numeric($key) ) 	$tmp[$key]= $val; # obtenemos
						}
						$aux[]= $tmp; # anidamos
					}
				}
				else {
					$b=sqlsrv_fetch_array($this->query, SQLSRV_FETCH_ASSOC);
					$tmp=array(); # inicializamos
					foreach( $b as $key=>$val ) {
						if( !is_numeric($key) ) 	$tmp[$key]= $val; # obtenemos
					}
					$aux[]= $tmp; # anidamos
				}
				$this->qResult= ((is_array($aux) && count($aux)) ? ($firtElement ? $aux[0][$firtElement]:$aux):NULL);
				unset($aux);
			}
			else {
				$this->qResult= ((is_array($a) && count($a)) ? pg_fetch_all($a):NULL);
			}
			$this->cleanDbCache();
		}
	}

	/**
	* retorna el resultado obtenido de una consulta
	*
	* @return mixed arreglo o json del resultado a retornar
	*/
	public function getResult($json=false) {
		return ($json ? json_encode($this->qResult):$this->qResult);
	}

	/**
	* retorna mensaje de error para consultas en Microsoft SQL Server
	*
	* @param array arreglo de datos generado por sqlsrv
	*
	* @return string mensaje en plano del error
	*/
	public function getMSSqlError($a=array()) {
		if( !is_array($a) )	{
			return false;
		}
		else {
			$aux='';
			foreach( $a as $key ) {
				$aux .= ($aux ? ', ':'').'['. $key["SQLSTATE"]. '] '. $key["code"]. ' '. $key["message"];
			}
			return $aux;
		}
	}

	/**
	* inserta en la base de datos
	*
	* @param string nombre de la tabla
	* @param array/string elementos a utilizar en el insert
	* @param boolen element para determinar el uso del autoconvertidor
	* @param string nombre de la conexion
	*/
	public function insert($tabla=NULL, $args=NULL, $autoconvert=true, $tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() ) {
			return false;
		}
		else {
			$in= ($autoconvert ? $this->arr2data($args, "insert"):$args); # convertir a insertables
			$q= 'insert into '. $this->sanitizeTableName($tabla). ' ('. $in["data"]. ') values('. $in["values"]. ')';

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($this->query= mysql_query($q, $this->dbLink[$tag])) ) {
					$this->setError("problemas para realizar consulta, razon: ". mysql_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consultar
					$this->setSucess("consultar realizada con exito", 2);
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($this->query=mysqli_query($this->dbLink[$tag], $q)) ) {
					$this->setError("problemas para realizar consulta, razon: ". mysqli_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				if( !($this->query=sqlsrv_query($this->dbLink[$tag], $q)) ) {
					$this->setError("problemas para realizar consulta, razon: ". $this->getMSSqlError(sqlsrv_errors()), 10);
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				if( !($this->query= $this->dbLink[$tag]->query($q)) ) {
					$this->setError("problemas para realizar consulta, razon: ", 10);
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
				}
			}
			else {
				$this->setError("el motor de base de datos indicado, no es soportado", 2);
			}
		}
	}

	/**
	* actualiza en la base de datos
	*
	* @param string nombre de la tabla
	* @param array/string elementos a utilizar en el insert
	* @param boolen element para determinar el uso del autoconvertidor
	* @param string nombre de la conexion
	*/
	public function update($tabla=NULL, $args=NULL, $autoconvert=true, $tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() )	return false;
		else {
			$in= ($autoconvert ? $this->arr2data($args, "update"):$args); # convertir a actualizable
			$q= 'update '. $this->sanitizeTableName($tabla). ' set '. $in["values"]. ' where '. $in["condition"];

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($this->query= mysql_query($q, $this->dbLink[$tag])) ) {
					$this->setError("problemas para realizar consulta, razon: ". mysql_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consultar
					$this->setSucess("consultar realizada con exito", 2);
					$this->setResult(mysql_fetch_array($this->query));
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($this->query=mysqli_query($this->dbLink[$tag], $q)) ) {
					$this->setError("problemas para realizar consulta, razon: ". mysqli_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
					$this->setResult(mysqli_fetch_array($this->query));
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				if( !($this->query=sqlsrv_query($this->dbLink[$tag], $q)) ) {
					$this->setError("problemas para realizar consulta, razon: ". $this->getMSSqlError(sqlsrv_errors()), 10);
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
					$this->setResult(sqlsrv_fetch_array($this->query, SQLSRV_FETCH_ASSOC));
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				if( !($this->query= $this->dbLink[$tag]->query($q)) ) {
					$this->setError("problemas para realizar consulta, razon: ", 10);
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
					$this->setResult($this->query);
				}
			}
			else {
				$this->setError("el motor de base de datos indicado, no es soportado", 2);
			}
		}
	}

	/**
	* elimina en la base de datos
	*
	* @param string nombre de la tabla
	* @param array/string elementos a utilizar en el insert
	* @param boolen element para determinar el uso del autoconvertidor
	* @param string nombre de la conexion
	*/
	public function delete($tabla=NULL, $args=NULL, $autoconvert=true, $tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		if( !$this->isDataNeededToConnect() )	return false;
		else {
			$in= ($autoconvert ? $this->arr2data($args, "delete"):$args); # convertir a eliminable
			$q= 'delete from '. $this->sanitizeTableName($tabla). ' where '. $in["values"];

			if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
				if( !($this->query= mysql_query($q, $this->dbLink[$tag])) ) {
					$this->setError("problemas para realizar consulta, razon: ". mysql_error($this->dbLink[$tag]), 10);
				}
				else { # exito de consultar
					$this->setSucess("consultar realizada con exito", 2);
					$this->setResult(mysql_fetch_array($this->query));
				}
			}
			else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
				if( !($this->query=mysqli_query($this->dbLink[$tag], $q)) ) {
					$this->setError("problemas para realizar consulta, razon: ". mysqli_error($this->dbLink[$tag]), 10);
					return 0;
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
					// $this->setResult(mysqli_fetch_array($this->query));
					return 1;
				}
			}
			else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
				if( !($this->query=sqlsrv_query($this->dbLink[$tag], $q)) )
					$this->setError("problemas para realizar consulta, razon: ". $this->getMSSqlError(sqlsrv_errors()), 10);
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
					$this->setResult(sqlsrv_fetch_array($this->query, SQLSRV_FETCH_ASSOC));
				}
			}
			else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
				if( !($this->query= $this->dbLink[$tag]->query($q)) ) {
					$this->setError("problemas para realizar consulta, razon: ", 10);
				}
				else { # exito de consulta
					$this->setSucess("consultar realizada con exito", 2);
					$this->setResult($this->query);
				}
			}
			else {  
				$this->setError("el motor de base de datos indicado, no es soportado", 2);
			}
		}
	}

	/**
	* convierte valores array a datos plano
	*
	* @param array datos a gestionar
	* @param string opciones adicionales
	*
	* @return string datos convertidos a planos
	*/
	public function arr2data($data=NULL, $op=NULL) {
		$r=0;
		if( !is_array($data) | !count($data) ) {
			return $r;
		}
		else {
			if( !strcmp($op, "insert") ) { # values to insert
				$r=array("data"=>'', "values"=>'');
				foreach( $data as $key=>$val ) {
					$r["data"] .= ($r["data"] ? ', ':''). $key;
					$r["values"] .= ($r["values"] ? ', ':''). $val;
				}
			}
			else if( !strcmp($op, "update") ) { # values to update
				$r=array("condition"=>'', "values"=>'');
				$i=0;
				foreach( $data as $key=>$val ) {
					$i++;
					if( $i==1 ) $r["condition"]= $key.'='.$val;
					$r["values"] .= ($r["values"] ? ', ':''). $key.'='.$val;
				}
				unset($i);
			}
			else if( !strcmp($op, "delete") ) { # values to delete
				$r=array("values"=>'');
				foreach( $data as $key=>$val ) {
					$r["values"] .= ($r["values"] ? ' && ':''). $key.'='.$val;
				}
			}
			else {
				$r=''; # generamos vacio
				foreach( $data as $key=>$val )
					$r .= ($r ? ' && ':''). $key. '='. $val;
			}
		}
		return $r;
	}

	/**
	* establece el nombre de usuario para conectar a la base de datos
	*
	* @param string nombre de usuario
	*/
	public function setUser($a=NULL) {
		$this->dbUser= ($a ? $a:NULL);
	}

	/**
	* retorna el nombre de usuario para conectar a la base de datos
	*
	* @return string nombre de usuario
	*/
	public function getUser() {
		return $this->dbUser;
	}

	/**
	* establece el password para conectar a la base de datos
	*
	* @param string password
	*/
	public function setPass($a=NULL) {
		$this->dbPass= ($a ? $a:NULL);
	}

	/**
	* retorna el password para conectar a la base de datos
	*
	* @return string password
	*/
	public function getPass() {
		return $this->dbPass;
	}

	/**
	* establece el puerto para conectar a la base de datos
	*
	* @param string el puerto de conexion
	*/
	public function setPort($a=NULL) {
		if( !strcmp($this->getDbEngine(), "mysql") ) { # MySQL
			$this->dbPort= ($a ? $a:self::MYSQL_PORT); # default 3309
		}
		else if( !strcmp($this->getDbEngine(), "mysqli") ) { # MySQLi
			$this->dbPort= ($a ? $a:self::MYSQL_PORT); # default 3309
		}
		else if( !strcmp($this->getDbEngine(), "mssql") ) { # Microsoft SQL
			$this->dbPort= ($a ? $a:self::MSSQL_PORT); # default 1433
		}
		else if( !strcmp($this->getDbEngine(), "psql") ) { # PostgreSQL
			$this->dbPort= ($a ? $a:self::POSTGRESQL_PORT); # default 5432
		}
		else { 
			$this->dbPort= NULL;
		}
	}

	/**
	* retorna el puerto de conexion a la base de datos
	*
	* @return string el puerto de conexion
	*/
	public function getPort() {
		return $this->dbPort;
	}	

	/**
	* establece el nombre del host donde esta la base de datos
	*
	* @param string direccion ip
	*/
	public function setHost($a=NULL) {
		if( !$a ) {
			$this->setError("no indico el nombre del host o IP", 4);
			$this->dbHost= NULL;
		}
		else {
			$patronIp= '/([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})/is';
			preg_match_all($patronIp, $a, $out);

			if( isset($out[0][0]) ) { # es una IP
				$b= gethostbyaddr($a);
				if( !$b )	$this->setError("el nombre del host o IP esta incorrectos", 3);
				$this->dbHost= ($b ? $out[0][0]:NULL); # ingreamos la IP
				unset($b);
			}
			else { # es Nombre de Host
				$b= gethostbyname($a);
				if( !$b )	$this->setError("el nombre del host o IP esta incorrectos", 3);
				$this->dbHost= ($b ? $b:NULL);
				unset($b);
			}
		}
	}

	/**
	* retorna la direccion IP del host donde esta la base de datos
	*
	* @return string direccion ip
	*/
	public function getHost() {
		return $this->dbHost;
	}

	/**
	* establece el nombre de la base de datos
	*
	* @param string nombre de la base de datos
	*/
	public function setDbName($a=NULL) {
		$this->dbName= ($a ? $a:NULL);
	}

	/**
	* retorna el nombre de la base de datos
	*
	* @return string nombre de la base de datos
	*/
	public function getDbName() {
		return $this->dbName;
	}

	/**
	* establece el nombre del recurso a conextar de la base de datos
	*
	* @param string nombre del recurso
	*/
	public function setDbResource($a=NULL) {
		$rs= (strstr($a, '\\\\') ? '':'\\\\');
		$this->dbResource= ($a ? $rs.$a:NULL);
	}

	/**
	* retorna el nombre del recurso a conextar de la base de datos
	*
	* @return string nombre del recurso
	*/
	public function getDbResource() {
		return $this->dbResource;
	}

	/**
	* retorna el identificador de la conexion
	*
	* @return object objeto de conexion a la base
	*/
	public function getLink($tagname=NULL) {
		$tag= (!$tagname ? 'default':$tagname);
		return $this->dbLink[$tag];
	}

	/**
	* establece mensaje de error con codigo (opcional)
	*
	* @param string mensaje de error
	* @param string codigo de error opcional
	*/
	public function setError($a=NULL, $code=NULL) {
		$this->error= ($a ? $a:NULL);
		$this->setErrorCode($code ? $code:NULL);
	}

	/**
	* establece codigo de mensaje de error
	*
	* @param string codigo de error
	*/
	public function setErrorCode($a=NULL) {
		$this->error_code= ($a ? $a:NULL);
	}

	/**
	* Retorna el mensaje de error con codigo (opcional)
	*
	* @return string mensaje de error
	*/
	public function getError() {
		return ($this->error_code ? $this->error_code.' - ':'').($this->error ? $this->error:NULL);
	}

	/**
	* establece mensaje de exito con codigo (opcional)
	*
	* @param string mensaje de exito
	* @param string codigo de exito opcional
	*/
	public function setSucess($a=NULL, $code=NULL) {
		$this->sucess= ($a ? $a:NULL);
		$this->setSucessCode($code ? $code:NULL);
	}

	/**
	* establece codigo de mensaje de exito
	*
	* @param string codigo de exito
	*/
	public function setSucessCode($a=NULL) {
		$this->sucess_code= ($a ? $a:NULL);
	}

	/**
	* retorna el mensaje de exito
	*
	* @return string mensaje de exito
	*/
	public function getSucess() {
		return ($this->sucess_code ? $this->sucess_code.' - ':'').($this->sucess ? $this->sucess:NULL);
	}

	/**
	* verifica si el motor de base de datos requerido es soportado\
	*
	* @param string nombre del motor de base de datos
	*
	* @return boolean falso o verdadero si coincide con los existentes
	*/
	public function isValidEngine($a=NULL) {
		if( !$a ) 	return 0;
		else {
			$fl=false;
			foreach( $this->dbList as $key )
				if( !strcmp($key, $a) && !$fl )	$fl=true;

			return ($fl ? 1:0);
		}
	}

	/**
	* Retorna el nombre de la base de datos que estamos trabajando
	*
	* @return string nombre de la base de datos
	*/
	public function getDbEngine() {
		return $this->dbEngine;
	}

	/**
	* Establece el nombre del motor de base de datos
	*
	* @param string nombre del motor de base de datos
	*/
	public function setDbEngine($a=NULL) {
		$this->dbEngine= $a;
	}

	/**
	* retorna el estado actual
	*
	* @return string mensaje del ultimo estado
	*/
	public function getEstado() {
		if( $this->getError() )	{
			return $this->getError();
		}
		else {
			$r= "Todo trabajando bien...";
			// if( $this->getSucess() )
			// 	echo "\n\nUltimo movimiento: ". $this->getSucess();
			return $r;
		}
	}

	/**
	* arma la estructura basica
	*
	* @param string nombre del motor de base de datos
	* @param string hostname o direccion IP
	* @param string nombre de usuario
	* @param string password
	* @param string nombre de la base de datos
	* @param string numero de puerto
	* @param string recurso para servidores Microsoft
	* @param bool configuracion para activar nombre de tablas en minusculas (defecto: falso)
	*/
	public function __construct($motor=NULL, $host=NULL, $user=NULL, $pass=NULL, $dbname=NULL, $puerto=NULL, $resource=NULL, $lowerCaseConfig=false, $indexName=NULL) {
		if( !$motor ) {
			$this->setError("no indico un motor de base de datos", 1);
		}
		else {
			if( !$this->isValidEngine($motor) ) {
				$this->setError("el motor de base de datos indicado, no es soportado", 2);
			}
			else {
				$this->setDbEngine($motor);
				$this->setHost($host);
				$this->setUser($user);
				$this->setPass($pass);
				$this->setDbName($dbname);
				$this->setPort($puerto);
				$this->setDbResource($resource);
				$this->setLowercaseTableNames($lowerCaseConfig);
				$this->setIndexName($indexName);
			}
		}
	}

	/**
	* elimina cualquier resago y cierra conexiones
	*/
	public function __destruct() {
		// echo "\n\n=== Destruyendo conexion..";
		if( $this->getDbEngine() ) {
			$this->closeDb();
		}
	}
}
?>
