<?php
// Configuración de la conexión a la base de datos
$dsn = 'mysql:host=TU_SERVIDOR;dbname=TU_BASE_DE_DATOS;charset=utf8mb4';
$username = 'TU_USUARIO';
$password = 'TU_CONTRASEÑA';

try {
    // Crear la conexión PDO
    $pdo = new PDO($dsn, $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Iniciar la transacción
    $pdo->beginTransaction();

    // Paso 1: Seleccionar el folio disponible más bajo con bloqueo
    $query = "
        SELECT ID, FOLIO 
        FROM FOLIO 
        WHERE USO = 1 
        ORDER BY FOLIO ASC 
        LIMIT 1
        FOR UPDATE";
    $stmt = $pdo->prepare($query);
    $stmt->execute();

    // Obtener el resultado
    $folio = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($folio) {
        // Paso 2: Actualizar la fila seleccionada
        $updateQuery = "
            UPDATE FOLIO 
            SET USO = 0, FECHA = NOW() 
            WHERE ID = :id";
        $updateStmt = $pdo->prepare($updateQuery);
        $updateStmt->execute(['id' => $folio['ID']]);

        // Confirmar la transacción
        $pdo->commit();

        // Devolver el folio asignado
        echo "Folio asignado: " . $folio['FOLIO'] . "\n";
    } else {
        // No hay folios disponibles
        echo "No hay folios disponibles.\n";

        // Cancelar la transacción
        $pdo->rollBack();
    }
} catch (PDOException $e) {
    // En caso de error, revertir la transacción
    $pdo->rollBack();
    echo "Error: " . $e->getMessage() . "\n";
}
?>