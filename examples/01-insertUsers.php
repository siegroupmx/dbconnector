#!/usr/bin/php
<?php
include( "../src/dbConnector.php" );
// $user= '__user__';
// $pass= '__pass__';
// $db= '__dbname__';
$user= 'admin';
$pass= 'gratis123';
$db= 'prueba';
$host= 'db';
$port= 3306; # tomar el default 3306

echo "### Testing on MySQL\n";

/**
* modo sencillo
*/
$db= new DBConnector("mysqli", $host, $user, $pass, $db, $port);
$db->conectar();

echo "\nEstado conexion: ". $db->getEstado();
echo "\nTipo de conexion: ". $db->getDbEngine();
echo "\nUsuario: ". $db->getUser();
echo "\nPassword: ". $db->getPass();
echo "\nHost: ". $db->getHost();
echo "\nDatabase Name: ". $db->getDbName(). "\n\n";

/**
* insertar datos
*/
$trama= array(
	"nombre"=>"'angel'", 
	"edad"=>"'20'", 
	"mail"=>"'angel@gmail.com'"
);

if( $db->insert("USUARIOS", $trama) ) {
	echo "\n=== Insertado con exito....\n";
	print_r($trama);
	echo "\n";
}

/**
* consulta simple
*/
$db->query(false, "USUARIOS");
// echo "\n\nEstado Consulta: ". $db->getEstado();
// echo "\nResultado RAW:\n";
// print_r($db->getResult());
echo "\nResultado JSON:\n";
$data= json_decode($db->getResult(true));
// print_r($data);

foreach( $data as $k=>$v ) {
	echo "\n======== ". $k. "\n";
	print_r($v);
	if( !$db->delete("USUARIOS", array("id"=>"'". $v->ID. "'")) ) {
		echo "\n-- Error al eliminar..";
	}
	else {
		echo "\n-- Eliminado con exito..";
	}

	echo "\n";
}
unset($db);

/**
* consultar RAW
*/
// $db->rawQuery("select * from USUARIOS where ID='4dm1n';");
// echo "\n\nEstado Consulta: ". $db->getEstado();
// echo "\nResultado RAW:\n";
// print_r($db->getResult());
// echo "\nResultado JSON:\n";
// print_r($db->getResult(true));

echo "\n\nEnd program...\n\n";
exit(0);
?>
