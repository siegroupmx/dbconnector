#!/usr/bin/php
<?php
include( "../src/dbConnector.php" );
// $user= '__user__';
// $pass= '__pass__';
// $db= '__dbname__';
$user= 'admin';
$pass= 'gratis123';
$db= 'prueba';
$host= 'db';
# $host= '127.0.0.1';
$port= 3306; # tomar el default 3306

echo "### Testing on MySQL\n";

/**
* modo sencillo
*/
$db= new DBConnector("mysqli", $host, $user, $pass, $db, $port);

/**
* modo amplio
*/
#$db= new DBConnector();
#$db->setDbEngine("mysql");
#$db->setHost($host);
#$db->setUser($user);
#$db->setPass($pass);
#$db->setPort($port);
#$db->setDbName($db);
#$db->setDbResource($resource); # solo microsoft sql server

$db->conectar();

echo "\nEstado conexion: ". $db->getEstado();
echo "\nTipo de conexion: ". $db->getDbEngine();
echo "\nUsuario: ". $db->getUser();
echo "\nPassword: ". $db->getPass();
echo "\nHost: ". $db->getHost();
echo "\nDatabase Name: ". $db->getDbName(). "\n\n";

/**
* consulta simple
*/
$db->query(false, "USUARIOS");
echo "\nEstado Consulta: ". $db->getEstado();
echo "\nResultado RAW:\n";
print_r($db->getResult());
echo "\nResultado JSON:\n";
print_r($db->getResult(true));

/**
* consultar RAW
*/
$db->rawQuery("select * from USUARIOS where ID='4dm1n';");
echo "\nEstado Consulta: ". $db->getEstado();
echo "\nResultado RAW:\n";
print_r($db->getResult());
echo "\nResultado JSON:\n";
print_r($db->getResult(true));

echo "\n\nEnd program...\n\n";
exit(0);
?>
